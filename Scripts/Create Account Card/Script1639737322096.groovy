import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Block/Login'), [('username') : login_email, ('password') : login_password], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Page - Home Page/a_Cards'))

WebUI.click(findTestObject('Page - Home Page/a_Your Cards'))

isElementPresent = WebUI.waitForElementPresent(findTestObject('Page - Cards/h3_You dont have any cards'), 5)

if (isElementPresent.equals(true)) {
    WebUI.comment('Jika Card Kosong Maka Create Card Baru')

    WebUI.click(findTestObject('Object Repository/Page - Cards/a_Create Card'))

    WebUI.selectOptionByLabel(findTestObject('Object Repository/Page - Cards/slc_Account'), account_name, false)

    WebUI.click(findTestObject('Object Repository/Page - Cards/btn_Create New Card'))
} else {
    WebUI.comment('Atau jika sudah ada, Maka Hapus Card')

    WebUI.click(findTestObject('Page - Cards/btn_Delete Card', [('no') : 1]))

    WebUI.click(findTestObject('Object Repository/Page - Cards/btn_Submit Delete Card'))
}

